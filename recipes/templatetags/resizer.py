from django import template
from recipes.models import Ingredient, Recipe

register = template.Library()

def resize_to(ingredient, target):
    number_of_servings = ingredient.recipe.servings

    if number_of_servings != None and target != None:
        try:
            ratio = int(target)/int(number_of_servings)
            return ratio * ingredient.amount
        except ValueError:
            pass
    return ingredient.amount

        # Get the number of servings from the ingredient's
    # recipe using the ingredient.recipe.servings
    # properties

    # If the servings from the recipe is not None
    #   and the value of target is not None
        # try
            # calculate the ratio of target over
            #   servings
            # return the ratio multiplied by the
            #   ingredient's amount
        # catch a possible error
            # pass
    # return the original ingre



register.filter(resize_to)
